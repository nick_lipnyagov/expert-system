# Expert system #

## Project description:
This project is a simple implementation of expert system based on backward chaining algorithm.

Algorithm explanation you may find [here](https://www.javatpoint.com/forward-chaining-and-backward-chaining-in-ai).

Current system selects which IT profession fits you the best based on your prefferences and then gives you an expert assessment.

**FYI:** Currently, It is implememnted the core functionality with user`s CLI interaction which may be adjusted for different samples of UI layer.

## How to run the project:

0. Open the project
1. Install packages with command `npm i` 
2. Run application with command `node app.js`

## Example of execution:

`What conclusion do you want to get? profession`

`What is the value for education?` 

`What is the value for skills? communicative`

`What is the value for english? intermediate`

`What is the value for character? like destroying`

`tester`