const KB = [
  {
    premises: [
      { attribute: 'education', value: 'technical' },
      { attribute: 'character', value: 'sitting' },
      { attribute: 'english', value: 'intermediate' },
      { attribute: 'skills', value: 'quick learner' },
    ],
    conclusion: { attribute: 'profession', value: 'programmer' }
  },
  {
    premises: [
      { attribute: 'education', value: 'art school' },
      { attribute: 'thinking', value: 'art' },
      { attribute: 'english', value: 'intermediate' },
      { attribute: 'character', value: 'creative' },
    ],
    conclusion: { attribute: 'profession', value: 'designer' }
  },
  {
    premises: [
      { attribute: 'skills', value: 'communicative' },
      { attribute: 'english', value: 'intermediate' },
      { attribute: 'character', value: 'like destroying' },
    ],
    conclusion: { attribute: 'profession', value: 'tester' }
  },
  {
    premises: [
      { attribute: 'thinking', value: 'strategic' },
      { attribute: 'english', value: 'advanced' },
      { attribute: 'skills', value: 'communicative' },
      { attribute: 'character', value: 'stress resistant' },
    ],
    conclusion: { attribute: 'profession', value: 'manager' }
  },
  {
    premises: [
      { attribute: 'thinking', value: 'analytical' },
      { attribute: 'english', value: 'advanced' },
      { attribute: 'character', value: 'scrupulous' },
    ],
    conclusion: { attribute: 'profession', value: 'business-analyst' }
  },
];

module.exports = KB;
