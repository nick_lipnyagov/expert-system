const readline = require('readline-sync');
const kb = require('./data/professions.js');

const backChain = function(goal, assertions) {
  // choose the first rule
  let ruleIndex = 0;
  // we begin with one statement of purpose
  assertions = assertions || [ goal ];

  // if there is a statement with the current goal as its attribute, choose it
  let assertion = assertions.filter(assertion => assertion.attribute === goal.attribute && assertion.value);
  assertion = assertion.length ? assertion[0] : null;

  if (!assertion) {
    while (!assertion && ruleIndex < kb.length) {
      const rule = kb[ruleIndex];

      if (rule.conclusion.attribute === goal.attribute) {
        let premiseIndex = 0;
        let allPremisesTrue = false;
        let premise = rule.premises[premiseIndex];
        let isPremiseAssertionTrue = true;

        while (!allPremisesTrue && isPremiseAssertionTrue) {
          // satisfy the current premise as the next goal
          trueAssertion = backChain(premise, assertions);

          // add a statement to the list of statements
          trueAssertion && !assertions.some(assertion => assertion.attribute === trueAssertion.attribute && assertion.value === trueAssertion.value) && assertions.push(trueAssertion);

          // whether the premise corresponds to the present statement
          isPremiseAssertionTrue = JSON.stringify(premise) === JSON.stringify(trueAssertion);
          if (isPremiseAssertionTrue) {
            // if there is more premise for pleasure, we pass to the following
            if (++premiseIndex < rule.premises.length) {
              premise = rule.premises[premiseIndex];
            }
            else {
              // all premise are satisfied, the current rule works
              allPremisesTrue = true;
            }
          }
        }

        if (allPremisesTrue) {
          // run the current rule and include its conclusion as a valid statement
          assertion = rule.conclusion;
        }
      }

      // choose the following rule
      ruleIndex++;
    }

    if (!assertion) {
      // it is not possible to determine the value for this purpose, if the goal is not the original query, we ask the user
      if (!assertions.filter(a => a.attribute === goal.attribute && !a.value).length) {
        // user request about the goal
        const value = readline.question(`What is the value for ${goal.attribute}? `);
        // enter new assertion for the type and value specified by the user
        assertion = { attribute: goal.attribute, value };
      }
    }
  }

  return assertion;
};

let goal = { attribute: readline.question('What conclusion do you want to get? ') };
assertion = backChain(goal);

if (assertion) {
  console.log(assertion.value);
}
else {
  console.log(`You can't answer enough questions to determine ${goal.attribute}.`);
}
